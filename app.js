'use strict';

const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const pokemons = require('./routes/pokemons');

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/pokemons', pokemons);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(function(req, res) {
  const err = new Error('Not Found');
  err.status = 404;
  res
    .status(err.status)
    .json({error: 'not found'});

});

app.use(function(err, req, res) {
  const message = err.message;
  const error = req.app.get('env') === 'development' ? err : {};
  res
    .status(err.status || 500)
    .json({error,  message});
});

module.exports = app;
