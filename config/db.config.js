'use strict';


const Sequelize = require('sequelize');

const prod = new Sequelize('Desafio-jz', 'DesafioAdmin', 'Picachu123', {
  host: 'jzd-dev-desafio.database.windows.net',
  dialect: 'mssql',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
  dialectOptions: {
    encrypt: true
  }
});


module.exports = prod;
