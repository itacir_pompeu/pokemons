'use strict';

const Pokemons = require('../models/pokemons');
const matchError = require('./helpers/macth_erros');

const R = require('ramda');

const create = (req, res) => {
  return Pokemons.create(req.body)
    .then(pokemons => res.status(201).json(pokemons))
    .catch(matchError(res));
};

const update = (req, res) => {
  const { id } = req.params;
  const { body } = req;
  const options = {
    where: { id }
  };

  if(R.has('type', body) || R.has('nivel', body)) {
    return res.send('403', {
      errors: ['only couch can be updated']
    });
  } 

  return Pokemons
    .update(body, options)
    .then(() => res.sendStatus(204))
    .catch(matchError(res));
};

const getOne = (req, res) => {
  const { id } =  req.params;

  return Pokemons
    .findById(id)
    .then(pokemon => res.json(pokemon || {}))
    .catch(matchError(res));
};

const get = (req, res) => {

  return Pokemons
    .findAll()
    .then(pokemons => res.json(pokemons || []))
    .catch(matchError(res));
};

const figth = (req, res) => {
  const { pokemonAId, pokemonBId } = req.params;

  return Pokemons
    .figth(pokemonAId, pokemonBId)
    .then(pokemons => res.json(pokemons || []))
    .catch(matchError(res));
};

module.exports = {
  create,
  update,
  getOne,
  get,
  figth
};
