'use strict';

const Sequelize = require('sequelize');
const sequelize = require('../config/db.config');
const R = require('ramda');

const fields = {
  type: {
    type: Sequelize.STRING,
    field: 'Tipo'
  },
  couch: {
    type: Sequelize.STRING,
    field: 'Treinador'
  },
  nivel: {
    type: Sequelize.INTEGER,
    defaultValue: 1,
    field: 'Nivel'
  }
};

const options = {
  freezeTableName: false,
  timestamps: false ,
  schema: 'ITACIR',
  tableName: 'Pokemons'
};

const Pokemons = sequelize.define('pokemons', fields, options);

Pokemons.sync();

Pokemons.figth = (firthId, secondoId) => {

  return Pokemons
    .findAll({ where: { id: [firthId, secondoId]}})
    .then(figthers => {

      const [firsth, second] = figthers;

      const list = R
        .concat(
            R.repeat(firsth, firsth.nivel),
            R.repeat(second, second.nivel)
            );

      const winner = R
        .head(R.sort(() => (.5 - Math.random()), list));

      const optionsWinner = {
        where: { id: winner.id}
      };

      const loser = R.head(R.filter(item => item.id !== winner.id, list));

        const optionsLosers = {
          where: { id: loser.id}
        };

      return Pokemons
        .findById(winner.id)
        .then(winner => {
          winner.nivel = winner.nivel + 1;
          return winner.save();
        })
        .then(winner => Pokemons
            .findById(loser.id)
            .then(loser => {
              loser.nivel = loser.nivel - 1;
              return loser.save()
                .then(() => removePokemoWhenHasNoNivel(R.clone(loser)))
                .then(() => {
                  return {
                    winner: winner.toJSON(),
                    loser: loser.toJSON()
                  }
                });
            }));

          });
};

const removePokemoWhenHasNoNivel = (pokemon) => {

  if (pokemon.get('nivel') > 0) {
    return Promise.resolve({});
  }

  return Pokemons
      .destroy({ where: { id: pokemon.get('id')}})
}

module.exports = Pokemons;
