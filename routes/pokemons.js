'use strict';

const express = require('express');
const router = express.Router();
const { create,
  update, 
  getOne, 
  get, 
  figth 
} = require('../controllers/pokemons_controller');

router
  .post('/', create)
  .put('/:id', update)
  .get('/:id', getOne)
  .get('/', get)
  .post('/figth/:pokemonAId/:pokemonBId', figth);

module.exports = router;
