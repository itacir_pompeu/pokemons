'use strict';

const request = require('supertest');
const expect = require('chai').expect;
const app = require('../../app');

const Pokemons = require('../../models/pokemons');

describe('Pokemons api module', () => {

  const pokemon = {
    type: 'pikachu',
    couch: 'Pompeu'
  };

  let id; 

  before(() => Pokemons
      .destroy({ where: {} }));

  describe('when /post in /pokemons', () => {

    it('should be create a pokemon', done => {

      request(app)
        .post('/pokemons')
        .send(pokemon)
        .expect('Content-Type', /json/)
        .expect(201)
        .end((err, res) => {
          expect(err).to.not.exist;
          expect(res.body.id).to.exist;
          id = res.body.id;
          expect(res.body.type).to.equal(pokemon.type);
          expect(res.body.couch).to.equal(pokemon.couch);
          expect(res.body.nivel).to.equal(1);
          done();
        });
    });

  });

  describe('when /put in /pokemons', () => {

    it('should be update a pokemon', done => {
      const body = {
        couch: 'Jose'
      };

      request(app)
        .put(`/pokemons/${id}`)
        .send(body)
        .expect(204)
        .end((err, res) => {
          expect(err).to.not.exist;
          expect(res.body).to.be.empty;
          return Pokemons.findById(id)
            .then(pokemon => {
              expect(pokemon.couch).to.eql(body.couch);
              done();
            });
        });
    });

    it('should be cant update only couch', done => {
      const body = {
        type: 'Charlizard',
        nivel: 200
      };

      request(app)
        .put(`/pokemons/${id}`)
        .send(body)
        .expect(403)
        .end((err, res) => {
          expect(err).to.not.exist;
          expect(res.body.errors).to.be.an.array;
          expect(res.body.errors).to.be.contains('only couch can be updated');
          return Pokemons.findById(id)
            .then(pokemon => {
              expect(pokemon.couch).to.not.eql(body.couch);
              done();
            });

          done();
        });
    });



  });

  describe('when /get with id in /pokemons', () => {

    it('should be get a pokemon', done => {
      request(app)
        .get(`/pokemons/${id}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {

          expect(err).to.not.exist;
          expect(res.body).to.exist;
          done();
        });
    });

  });

  describe('when /get in /pokemons', () => {

    it('should be get a pokemon', done => {
      request(app)
        .get(`/pokemons/`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {

          expect(err).to.not.exist;
          expect(res.body).to.exist;
          console.log(res.body)
            expect(res.body).to.be.an('array');
          done();
        });
    });

  });


  describe('when /post in /pokemons/figth', () => {
    const secondPekemon = {
      type: 'pikachu',
      couch: 'Pompeu'
    };

    let secondoId;

    before(() => Pokemons
        .create(secondPekemon)
        .then(newSecondPekemon => secondoId = newSecondPekemon.get('id')));


    it('should be a pokemon fighting', done => {
      const url = `/pokemons/figth/${id}/${secondoId}`;

      request(app)
        .post(url)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          const { body } = res;
          expect(err).to.not.exist;
          expect(body).to.exist;

          expect(body['winner']).to.an.object;
          expect(body['winner'].nivel).to.be.eql(2);
          expect(body['loser']).to.an.object;
          expect(body['loser'].nivel).to.be.eql(0);
          done();
        });
    });

  });

});
