'use strict';

const expect = require('chai').expect;
const Pokemons = require('../../models/pokemons');

describe('Model Pokemons', () => {
  describe('atributs verify', () => {
    const pokemon = {
      type: 'pikachu',
      couch: 'Pompeu',
    };

    const secondPekemon = {
      type: 'bubasauro',
      couch: 'limp'
    }

    let firthId;

    before(() => Pokemons.destroy({ where: {} }));

    it('should be exist', () => {
      expect(Pokemons).to.exist;
    });

    it('should be pokemons has id, type, couch, nivel', () => {
      return Pokemons
        .create(pokemon)
        .then(newPokemon => {
          firthId = newPokemon.get('id');
          expect(newPokemon.get('id')).to.not.equal(1);
          expect(newPokemon.get('type')).to.equal(pokemon.type);
          expect(newPokemon.get('couch')).to.equal(pokemon.couch);
          expect(newPokemon.get('nivel')).to.equal(1);
        });
    });

    it('should be two pokemon module has figth method', () => {
      return Pokemons
        .create(secondPekemon)
        .then(newSecondPekemon => {

          return Pokemons
            .figth(firthId, newSecondPekemon.get('id'))
            .then(result => {
              expect(result).to.be.an.object; 
              expect(result['winner']).to.be.exist; 
              expect(result['loser']).to.be.exist; 

              expect(result['winner'].nivel).to.be.eql(2); 
              expect(result['loser'].nivel).to.be.to.be.eql(0); 

              return Pokemons
                .findById(result['loser'].id)
                .then(loser => {
                  expect(loser).to.be.null;
                });
            });

        });
    });

  });
});
